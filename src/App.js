import {
  Button,
  HStack,
  Box,
  Tag,
  Text,
  Heading,
  Spinner,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Stack,
  Select,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from "@chakra-ui/react";
import downloadCsv from "download-csv";
import { format } from "date-fns";
import { useEffect, useState } from "react";
import audio from "./alarm.mp3";

function App() {
  const [isAlarmTurnOn, setIsAlarmTurnOn] = useState(false);
  const [isDeviceTurnOn, setIsDeviceTurnOn] = useState(false);
  const [data, setData] = useState([]);
  const [logs, setLogs] = useState([]);
  const [rooms, setRooms] = useState([
    { id: "1", name: "Sala" },
    { id: "2", name: "Cozinha" },
    { id: "3", name: "Banheiro" },
    { id: "4", name: "Suíte" },
    { id: "5", name: "Quarto " },
  ]);
  const [devicesIn, setDevicesIn] = useState([
    { id: "1", room: "1", name: "Interruptor Sala 1" },
    { id: "2", room: "1", name: "Interruptor Sala 2" },
    { id: "3", room: "2", name: "Interruptor Cozinha 1" },
    { id: "4", room: "2", name: "Interruptor Cozinha 2" },
    { id: "5", room: "3", name: "Interruptor Banheiro" },
    { id: "6", room: "4", name: "Interruptor Suíte" },
    { id: "7", room: "5", name: "Interruptor Quarto 1" },
    { id: "8", room: "5", name: "Interruptor Quarto 2" },
    { id: "9", room: "5", name: "Sensor Porta Quarto 1" },
    { id: "10", room: "5", name: "Sensor Porta Quarto 2" },
    { id: "11", room: "1", name: "Sensor Porta Sala" },
    { id: "12", room: "2", name: "Sensor Porta Cozinha" },
    { id: "13", room: "4", name: "Sensor Porta Suíte" },
    { id: "14", room: "3", name: "Sensor Porta Banheiro" },
    { id: "15", room: "5", name: "Sensor Janela Quarto 1" },
    { id: "16", room: "5", name: "Sensor Janela Quarto 2" },
    { id: "17", room: "1", name: "Sensor Janela Sala" },
    { id: "18", room: "2", name: "Sensor Janela Cozinha" },
    { id: "19", room: "4", name: "Sensor Janela Suíte" },
    { id: "20", room: "3", name: "Sensor Janela Banheiro" },
  ]);

  const [devicesOut, setDevicesOut] = useState([
    { id: "1", room: "1", name: "Lâmpada Sala 1" },
    { id: "2", room: "1", name: "Lâmpada Sala 2" },
    { id: "3", room: "2", name: "Lâmpada Cozinha 1" },
    { id: "4", room: "2", name: "Lâmpada Cozinha 2" },
    { id: "5", room: "3", name: "Lâmpada Banheiro" },
    { id: "6", room: "4", name: "Lâmpada Suíte" },
    { id: "7", room: "5", name: "Lâmpada Quarto 1" },
    { id: "8", room: "5", name: "Lâmpada Quarto 2" },
    { id: "9", room: "5", name: "Ar-condicionado Quarto" },
    { id: "10", room: "1", name: "Ar-condicionado Sala" },
  ]);

  const deviceModel = useDisclosure();
  const logModel = useDisclosure();
  const alertModel = useDisclosure();

  useEffect(() => {
    init();
  }, []);

  const init = () => {
    setLogs([]);
    setTimeout(function () {
      setData([
        {
          id: "AS78!@S",
          umidade: 50,
          temperatura: 30,
          local: "Sala",
          entrada: "Interruptor",
          saida: "Lâmpada",
        },
      ]);
    }, 1500);
  };

  const alertUser = () => {
    const alarm = new Audio(audio);
    alertModel.onOpen();
    alarm.play();
  };

  const toggleAlarm = () => {
    const message = isAlarmTurnOn
      ? "Alarme foi desligado"
      : "Alarme foi ligado";
    setLogs([
      ...logs,
      { message, time: format(new Date(), "yyyy-MM-dd 'às' HH:mm:ss") },
    ]);
    setIsAlarmTurnOn(!isAlarmTurnOn);
  };

  const toggleDevice = (id) => {
    const message = isDeviceTurnOn
      ? `Device ${id} foi desconectado`
      : `Device ${id} foi conectado`;
    setLogs([
      ...logs,
      { message, time: format(new Date(), "yyyy-MM-dd 'às' HH:mm:ss") },
    ]);
    setIsDeviceTurnOn(!isDeviceTurnOn);
  };

  const exportLog = () => {
    downloadCsv(
      logs,
      { message: "Mensagem", time: "hora" },
      "logProjetoFinal.csv"
    );
    logModel.onClose();
  };

  const renderCard = (device, key) => (
    <Box
      bgColor="white"
      p="1.3rem"
      borderRadius="16px"
      boxShadow="3px 3px 3px #35353550"
      key={key}
    >
      <HStack justifyContent="space-between">
        <div>
          <Text fontSize="xl">
            {device && device["id"]
              ? `Dispositivo Id - ${device["id"]}`
              : "Não informado"}
          </Text>
          <div>
            <Tag colorScheme={isDeviceTurnOn ? "green" : "red"}>
              {isDeviceTurnOn ? "Conectado" : "Desconectado"}
            </Tag>
          </div>
        </div>
        <div>
          <Button
            colorScheme={isDeviceTurnOn ? "red" : "green"}
            onClick={() => toggleDevice(device.id)}
          >
            {isDeviceTurnOn ? "Desconectar" : "Conectar"}
          </Button>
        </div>
      </HStack>

      <HStack mt="2rem">
        <Text fontSize="xl" pr="1rem">
          <b>Umidade:</b>{" "}
          {device && device["umidade"]
            ? `${device["umidade"]}%`
            : "Não informado"}
        </Text>
        <Text fontSize="xl" pr="1rem">
          <b>Temperatura:</b>{" "}
          {device && device["temperatura"]
            ? `${device["temperatura"]}°C`
            : "Não informado"}
        </Text>
        <Text fontSize="xl" pr="1rem">
          <b>Local:</b>{" "}
          {device && device["local"] ? device["local"] : "Não informado"}
        </Text>
        <Text fontSize="xl" pr="1rem">
          <b>Entrada:</b>{" "}
          {device && device["entrada"] ? device["entrada"] : "Não informado"}
        </Text>
        <Text fontSize="xl" pr="1rem">
          <b>Saída:</b>{" "}
          {device && device["saida"] ? device["saida"] : "Não informado"}
        </Text>
      </HStack>
    </Box>
  );

  const renderDeviceModal = () => (
    <Modal isOpen={deviceModel.isOpen} onClose={deviceModel.onClose} size="xl">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Cadastrar Dispositivo</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Stack spacing={3}>
            <Select placeholder="Local do dispositivo">
              {rooms.map(({ id, name }) => (
                <option value={id} key={id}>
                  {name}
                </option>
              ))}
            </Select>
            <Select placeholder="Entrada do dispositivo">
              {devicesIn.map(({ id, name }) => (
                <option value={id} key={id}>
                  {name}
                </option>
              ))}
            </Select>
            <Select placeholder="Saída do dispositivo">
              {devicesOut.map(({ id, name }) => (
                <option value={id} key={id}>
                  {name}
                </option>
              ))}
            </Select>
          </Stack>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" onClick={deviceModel.onClose}>
            Salvar
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );

  const renderLogModal = () => (
    <Modal isOpen={logModel.isOpen} onClose={logModel.onClose} size="xl">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Log</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Table variant="striped" colorScheme="blue" size="sm">
            <Thead>
              <Tr>
                <Th>Ação</Th>
                <Th>Hora</Th>
              </Tr>
            </Thead>
            <Tbody>
              {logs.length ? (
                logs?.map((log, i) => (
                  <Tr key={i}>
                    <Td>{log.message}</Td>
                    <Td>{log.time}</Td>
                  </Tr>
                ))
              ) : (
                <Tr>
                  <Td>Sem log no momento</Td>
                  <Td>Sem log no momento</Td>
                </Tr>
              )}
            </Tbody>
          </Table>
        </ModalBody>

        <ModalFooter>
          <Button
            colorScheme="blue"
            disabled={!logs.length}
            onClick={exportLog}
          >
            Baixar
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );

  const renderAlertModal = () => (
    <Modal isOpen={alertModel.isOpen} onClose={alertModel.onClose} size="xl">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Alerta</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Heading textAlign="center" color="red.400">
            Alguém entrou na casa
          </Heading>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" onClick={alertModel.onClose}>
            Certo
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );

  return (
    <Box pb="1rem">
      <Box bgColor="blue.500" color="white" p="1rem 2rem">
        <Heading>Trabalho Final FSE</Heading>
      </Box>
      <Box mx="2rem">
        <HStack justifyContent="space-between" my="2rem">
          <Text
            fontSize="xl"
            fontWeight="bold"
            color={!isAlarmTurnOn ? "red" : "green"}
          >
            {isAlarmTurnOn ? "Alarme ligado" : "Alarme desligado"}
          </Text>
          <div>
            <Button colorScheme="red" mr="1rem" onClick={alertUser}>
              Alarme
            </Button>
            <Button
              colorScheme={isAlarmTurnOn ? "red" : "green"}
              mr="1rem"
              variant="outline"
              onClick={toggleAlarm}
            >
              {isAlarmTurnOn ? "Desligar Alarme" : "Ligar Alarme"}
            </Button>
            <Button
              colorScheme="blue"
              mr="1rem"
              variant="outline"
              onClick={logModel.onOpen}
            >
              Log
            </Button>
            <Button colorScheme="blue" onClick={deviceModel.onOpen}>
              Caddastrar Dispositivos
            </Button>
          </div>
        </HStack>
        {renderDeviceModal()}
        {renderLogModal()}
        {renderAlertModal()}
        <div>
          {data.length ? (
            data?.map((device, i) => renderCard(device, i))
          ) : (
            <HStack>
              <Spinner mr="1rem" /> <Heading>Careegando os dados</Heading>
            </HStack>
          )}
        </div>
      </Box>
    </Box>
  );
}

export default App;
